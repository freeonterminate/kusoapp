(*
 * JsonLoader Class
 *
 * PLATFORMS
 *   Windows / macOS / iOS / Android / Linux
 *
 * ENVIRONMENT
 *   Delphi 10.3.x Rio
 *
 * LICENSE
 *   Copyright (C) 2019 HOSOKAWA Jun (Twitter: @pik)
 *   Released under the MIT license
 *   http://opensource.org/licenses/mit-license.php
 *
 * HISTORY
 *   2019/07/01 Version 1.0.0
 *
 * USAGE
 *   type
 *     TJsonType = class
 *       Baz: String;
 *     end;
 *
 *   var Loader := TJsonLoader<TJsonType>.Create;
 *   Loader.Execute(
 *     'http://foo.bar/sample.json',
 *     True, // True: the generated JSON instance will be released automatically
 *     procedure (const iSuccess: Boolean; const iResult: TJsonType)
 *     begin
 *       if (iSuccess) then
 *         Writeln(iResult.Baz)
 *       else
 *         Writeln('Failed.');
 *
 *       Loader.DisposeOf;
 *     end
 *   );
 *)

 unit PK.Net.JSON.Loader;

interface

uses
  System.JSON.Types
  , System.JSON.Serializers
  , System.Net.URLClient
  , System.Net.HttpClient
  ;

type
  // TJsonLoader で読み出した直後に特殊な処理をしたい場合に継承する
  TJsonLoaderClient = class
  protected
    procedure Loaded; virtual; abstract; // Deserialize 直後に呼び出される
  end;

  TJsonLoader<T: class> = class
  public type
    TResultProc =
      reference to procedure (const iSuccess: Boolean; const iResult: T);
    TStartLoadingProc =
      reference to procedure (const iHttpClient: THttpClient);
    TStartLoadingEvent = procedure (const iHttpClient: THttpClient) of object;
    TLoadedEvent =
      procedure (const iSuccess: Boolean; const iResult: T) of object;
  private var
    FResponse: T;
    FAutoRelease: Boolean;
    FHttpClient: THttpClient;
    FErrorMsg: String;
    FErrorCode: Integer;
    FOnStartLoading: TStartLoadingEvent;
    FOnLoaded: TLoadedEvent;
  private
    procedure CallEvent(
      const iSuccess: Boolean;
      const iValue: T;
      const iProc: TResultProc);
  public
    constructor Create; reintroduce; virtual;
    destructor Destroy; override;
    procedure Execute(
      const iURI: TURI;
      const iAutoRelease: Boolean;
      const iProc: TResultProc;
      const iStartLoadingProc: TStartLoadingProc = nil); overload;
    procedure Execute(
      const iURL: String;
      const iAutoRelease: Boolean;
      const iProc: TResultProc;
      const iStartLoadingProc: TStartLoadingProc = nil); overload;
  public
    property HttpClient: THttpClient read FHttpClient;
    property Response: T read FResponse;
    property ErrorCode: Integer read FErrorCode;
    property ErrorMsg: String read FErrorMsg;
    property OnStartLoading: TStartLoadingEvent
      read FOnStartLoading write FOnStartLoading;
    property OnLoaded: TLoadedEvent read FOnLoaded write FOnLoaded;
  end;

implementation

uses
  System.Types
  , System.Classes
  , System.SysUtils
  ;

{ TJsonLoader<T> }

procedure TJsonLoader<T>.CallEvent(
  const iSuccess: Boolean;
  const iValue: T;
  const iProc: TResultProc);
begin
  FResponse := iValue;

  TThread.ForceQueue(
    nil,
    procedure
    begin
      if Assigned(FOnLoaded) then
        FOnLoaded(iSuccess, iValue);

      if Assigned(iProc) then
        iProc(iSuccess, iValue);

      if iSuccess and FAutoRelease then
        iValue.DisposeOf;
    end
  );
end;

constructor TJsonLoader<T>.Create;
begin
  inherited Create;

  FHttpClient := THttpClient.Create;
end;

destructor TJsonLoader<T>.Destroy;
begin
  FHttpClient.DisposeOf;

  inherited;
end;

procedure TJsonLoader<T>.Execute(
  const iURL: String;
  const iAutoRelease: Boolean;
  const iProc: TResultProc;
  const iStartLoadingProc: TStartLoadingProc = nil);
begin
  if FHttpClient <> nil then
    FHttpClient.DisposeOf;

  TThread.CreateAnonymousThread(
    procedure
    begin
      var Def: T := Default(T);

      // HttpClient 生成
      FHttpClient := THttpClient.Create;
      FHttpClient.HandleRedirects := True;

      if Assigned(FOnStartLoading) then
        FOnStartLoading(FHttpClient);

      if Assigned(iStartLoadingProc) then
        iStartLoadingProc(FHttpClient);

      // 呼び出し
      var Response := FHttpClient.Get(iURL);

      // 結果チェック
      if Response = nil then
      begin
        CallEvent(False, Def, iProc);
        Exit;
      end;

      var Res := Response.ContentAsString(TEncoding.UTF8);
      if Res.IsEmpty then
      begin
        CallEvent(False, Def, iProc);
        Exit;
      end;

      FAutoRelease := iAutoRelease;

      var R := Def;
      var S := TJsonSerializer.Create;
      try
        try
          R := S.Deserialize<T>(Res);

          // Json 変数の初期化機会を与える
          if R is TJsonLoaderClient then
            TJsonLoaderClient(R).Loaded;

          // 成功
          CallEvent(True, R, iProc);
        except
          CallEvent(False, Def, iProc);
        end;
      finally
        S.DisposeOf;
      end;
    end
  ).Start;
end;

procedure TJsonLoader<T>.Execute(
  const iURI: TURI;
  const iAutoRelease: Boolean;
  const iProc: TResultProc;
  const iStartLoadingProc: TStartLoadingProc = nil);
begin
  Execute(iURI.ToString, iAutoRelease, iProc, iStartLoadingProc);
end;

end.
