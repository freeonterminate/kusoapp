﻿(*
 * Form Utils
 *
 * PLATFORMS
 *   Windows / macOS
 *
 * LICENSE
 *   Copyright (c) 2018 HOSOKAWA Jun
 *   Released under the MIT license
 *   http://opensource.org/licenses/mit-license.php
 *
 * 2018/04/08 Version 1.0.0
 * 2018/05/13 Version 1.0.1  Rename Show -> BringToFront
 * 2018/05/21 Version 1.0.2  BringToFront overloaded (Only Windows)
 * Programmed by HOSOKAWA Jun (twitter: @pik)
 *)

unit PK.Utils.Form;

interface

uses
  FMX.Forms;

type
  TFormState = (Normal, Minimize);
  TFormStateNotifyProc = reference to procedure(const iState: TFormState);

  IFormUtils = interface
    ['{C7D3C846-1FA8-47E7-B0D4-F94D0022A85A}']
    function FindWindow(const iCaption: String): NativeInt;
    procedure HideTaskbar;
    procedure ShowTaskbar;
    procedure BringToFront; overload;
    procedure BringToFront(const iWnd: NativeUInt); overload;
    procedure RegistMinimizeProc(
      const iForm: TCommonCustomForm;
      const iProc: TFormStateNotifyProc);
  end;

  IFormUtilsFactory = interface
    ['{17320B02-74E5-4C1B-A64F-122FCD2EE896}']
    function CreateFormUtils: IFormUtils;
  end;

  TFormUtilsFactory = class(TInterfacedObject, IFormUtilsFactory)
  public
    function CreateFormUtils: IFormUtils; virtual; abstract;
  end;

  TFormUtils = class
  private var
    FFormUtils: IFormUtils;
  public
    constructor Create; reintroduce;
    destructor Destroy; override;
    function FindWindow(const iCaption: String): NativeInt;
    procedure HideTaskbar;
    procedure ShowTaskbar;
    procedure BringToFront; overload;
    procedure BringToFront(const iWnd: NativeUInt); overload;
    procedure RegisterMinimizeProc(
      const iForm: TCommonCustomForm;
      const iProc: TFormStateNotifyProc);
  end;

implementation

uses
  FMX.Platform
  {$IFDEF MSWINDOWS}
  , PK.Utils.Form.Win
  {$ENDIF}
  {$IFDEF OSX}
  , PK.Utils.Form.Mac
  {$ENDIF}
  ;

{ TFormUtils }

procedure TFormUtils.BringToFront;
begin
  if FFormUtils <> nil then
    FFormUtils.BringToFront;
end;

procedure TFormUtils.BringToFront(const iWnd: NativeUInt);
begin
  if FFormUtils <> nil then
    FFormUtils.BringToFront(iWnd);
end;

constructor TFormUtils.Create;
var
  FormUtilsFactory: IFormUtilsFactory;
begin
  inherited Create;

  if
    (
      TPlatformServices.Current.SupportsPlatformService(
        IFormUtilsFactory,
        IInterface(FormUtilsFactory)
      )
    )
  then
    FFormUtils := FormUtilsFactory.CreateFormUtils;
end;

destructor TFormUtils.Destroy;
begin
  FFormUtils := nil;

  inherited;
end;

function TFormUtils.FindWindow(const iCaption: String): NativeInt;
begin
  if FFormUtils = nil then
    Result := 0
  else
    Result := FFormUtils.FindWindow(iCaption);
end;

procedure TFormUtils.HideTaskbar;
begin
  if FFormUtils <> nil then
    FFormUtils.HideTaskbar;
end;

procedure TFormUtils.RegisterMinimizeProc(
  const iForm: TCommonCustomForm;
  const iProc: TFormStateNotifyProc);
begin
  if FFormUtils <> nil then
    FFormUtils.RegistMinimizeProc(iForm, iProc);
end;

procedure TFormUtils.ShowTaskbar;
begin
  if FFormUtils <> nil then
    FFormUtils.ShowTaskbar;
end;

end.
