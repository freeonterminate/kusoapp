﻿(*
 * Form Utils
 *
 * PLATFORMS
 *   macOS
 *
 * LICENSE
 *   Copyright (c) 2018 HOSOKAWA Jun
 *   Released under the MIT license
 *   http://opensource.org/licenses/mit-license.php
 *
 * 2018/04/08 Version 1.0.0
 * 2018/05/13 Version 1.0.1  Rename Show -> BringToFront
 * 2018/05/21 Version 1.0.2  BringToFront overloaded (Only Windows)
 * 2019/11/27 Version 1.0.3  macOS Catalina support
 * Programmed by HOSOKAWA Jun (twitter: @pik)
 *)

unit PK.Utils.Form.Mac;

{$IFNDEF OSX}
{$WARNINGS OFF 1011}
interface
implementation
end.
{$ENDIF}

interface

implementation

uses
  System.Classes
  , System.SysUtils
  , Macapi.ObjectiveC
  , Macapi.AppKit
  , Macapi.Foundation
  , Macapi.Helpers
  , FMX.Helpers.Mac
  , FMX.Forms
  , FMX.Platform
  , FMX.Platform.Mac
  , PK.Utils.Form
  ;

type
  TFormUtilsMac = class(TInterfacedObject, IFormUtils)
  private var
    FNSApp: NSApplication;
    FRegsitedForm: TCommonCustomForm;
    FRegistedProc: TFormStateNotifyProc;
  public
    constructor Create; reintroduce;
    function FindWindow(const iCaption: String): NativeInt;
    procedure HideTaskbar;
    procedure ShowTaskbar;
    procedure BringToFront; overload;
    procedure BringToFront(const iWnd: NativeUInt); overload;
    procedure RegistMinimizeProc(
      const iForm: TCommonCustomForm;
      const iProc: TFormStateNotifyProc);
  end;

  TFormUtilsFactoryMac = class(TFormUtilsFactory)
  public
    function CreateFormUtils: IFormUtils; override;
  end;

procedure RegisterFormUtilsMac;
begin
  TPlatformServices.Current.AddPlatformService(
    IFormUtilsFactory,
    TFormUtilsFactoryMac.Create);
end;

{ TFormUtilsMac }

procedure TFormUtilsMac.BringToFront;
var
  App: NSApplication;
begin
  App := TNSApplication.Wrap(TNSApplication.OCClass.sharedApplication);
  App.activateIgnoringOtherApps(True);
  App.mainWindow.makeKeyAndOrderFront(nil);

  Application.MainForm.Show;
end;

procedure TFormUtilsMac.BringToFront(const iWnd: NativeUInt);
begin
end;

constructor TFormUtilsMac.Create;
begin
  inherited Create;

  FNSApp := TNSApplication.Wrap(TNSApplication.OCClass.sharedApplication);
end;

function TFormUtilsMac.FindWindow(const iCaption: String): NativeInt;
var
  Apps: NSArray;
begin
  Apps :=
    TNSRunningApplication
    .OCClass
    .runningApplicationsWithBundleIdentifier(StrToNSStr(iCaption));

  if Apps.count > 0 then
    Result := -1
  else
    Result := 0;
end;

procedure TFormUtilsMac.HideTaskbar;
begin
  FNSApp.setActivationPolicy(NSApplicationActivationPolicyAccessory);
end;

procedure TFormUtilsMac.RegistMinimizeProc(
  const iForm: TCommonCustomForm;
  const iProc: TFormStateNotifyProc);
begin
  FRegsitedForm := iForm;
  FRegistedProc := iProc;
end;

procedure TFormUtilsMac.ShowTaskbar;
begin
  FNSApp.setActivationPolicy(NSApplicationActivationPolicyRegular);
end;

{ TFormUtilsFactoryMac }

function TFormUtilsFactoryMac.CreateFormUtils: IFormUtils;
begin
  Result := TFormUtilsMac.Create;
end;

initialization
  RegisterFormUtilsMac;

end.
