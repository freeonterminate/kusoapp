﻿(*
 * Browser Utils
 *
 * PLATFORMS
 *   Windows / macOS / iOS / Android
 *
 * LICENSE
 *   Copyright (c) 2018 HOSOKAWA Jun
 *   Released under the MIT license
 *   http://opensource.org/licenses/mit-license.php
 *
 * 2015/11/27 Version 1.0.0
 * Programmed by HOSOKAWA Jun (twitter: @pik)
 *)

unit PK.Utils.Browser;

interface

procedure OpenBrowser(const iURL: String);

implementation

uses
  System.SysUtils

  {$IFDEF MSWINDOWS}
    , Winapi.Windows, Winapi.ShellAPI
  {$ENDIF}

  {$IFDEF ANDROID}
    , Androidapi.JNI.Net
    , Androidapi.JNI.App
    , Androidapi.JNI.GraphicsContentViewText
    , Androidapi.Helpers
    , FMX.Helpers.Android
  {$ENDIF}

  {$IFDEF MACOS}
    {$IFDEF IOS}
      , Macapi.Helpers
      , iOSapi.Foundation
      , FMX.Helpers.IOS
    {$ELSE}
      , Posix.Stdlib
    {$ENDIF}
  {$ENDIF}
  ;

{$IFDEF MSWINDOWS}
procedure OpenBrowser(const iURL: String);
begin
  if (ShellExecute(0, 'open', PChar(iURL), nil, nil, SW_SHOW) < 32) then
    WinExec(PAnsiChar(AnsiString('explorer ' + iURL)), SW_SHOW)
end;
{$ENDIF}

{$IF defined(MACOS) and not defined(IOS)}
procedure OpenBrowser(const iURL: String);
begin
  _system(PAnsiChar(AnsiString('open ' + iURL)));
end;
{$ENDIF}

{$IFDEF ANDROID}
procedure OpenBrowser(const iURL: String);
var
  Intent: JIntent;
begin
  Intent :=
    TJIntent.JavaClass.init(
      TJIntent.JavaClass.ACTION_VIEW,
      StrToJURI(iURL));

  {$IF RTLVersion < 30}
  SharedActivity.startActivity(Intent);
  {$ELSE}
  TAndroidHelper.Activity.startActivity(Intent);
  {$ENDIF}
end;
{$ENDIF}

{$IFDEF IOS}
procedure OpenBrowser(const iURL: String);
begin
  SharedApplication.openURL(
    TNSURL.Wrap(TNSURL.OCClass.URLWithString(StrToNSStr(iURL))));
end;
{$ENDIF}

end.
