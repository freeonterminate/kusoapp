﻿(*
 * KusoApp
 *
 * PLATFORMS
 *   Windows / macOS
 *
 * 2019/12/01 Version 1.0
 * Programmed by pik
 *)

unit uAbout;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.StdCtrls,
  FMX.Controls.Presentation, FMX.Objects, FMX.Layouts, FMX.Effects,
  FMX.Filter.Effects, FMX.Ani;

type
  TfrmAbout = class(TForm)
    layoutRoot: TLayout;
    imgUnko: TImage;
    layoutImageBase: TLayout;
    layoutTextBase: TLayout;
    layoutButtonBase: TLayout;
    btnClose: TButton;
    styleClear: TStyleBook;
    lblTitle: TLabel;
    lblVersion: TLabel;
    lblAuthor: TLabel;
    lineTitleSep: TLine;
    layoutTitleBase: TLayout;
    lblExplain: TLabel;
    effectRipple: TRippleEffect;
    animRipple: TFloatAnimation;
    animWave: TFloatAnimation;
    effectWave: TWaveEffect;
    procedure animRippleProcess(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private var
    FDirX: Single;
    FDirY: Single;
    FWidth: Single;
    FHeight: Single;
    FCenter: TPointF;
  private
    procedure SetBitmap(const iBmp: TBitmap);
  public
    class procedure ShowSelf(const iBmp: TBitmap);
  end;

implementation

{$R *.fmx}

{ TfrmAbout }

procedure TfrmAbout.animRippleProcess(Sender: TObject);
begin
  FCenter := PointF(FCenter.X + FDirX, FCenter.Y + FDirY);
  effectRipple.Center := FCenter;

  if (FCenter.X <= 0) or (FCenter.X >= FWidth)  then
    FDirX := -FDirX;

  if (FCenter.Y <= 0) or (FCenter.Y >= FHeight)  then
    FDirY := -FDirY;
end;

procedure TfrmAbout.FormCreate(Sender: TObject);
begin
  Left := Application.MainForm.Left + 96;
  Top := Application.MainForm.Bounds.Bottom;
end;

procedure TfrmAbout.SetBitmap(const iBmp: TBitmap);
begin
  imgUnko.Bitmap.Assign(iBmp);
  FWidth := iBmp.Width;
  FHeight := layoutImageBase.Height;

  var CX := FWidth / 2;
  var CY := FHeight / 2;
  CX := CX + Random(Trunc(CX));
  CY := CY + Random(Trunc(CY));
  FCenter := PointF(CX, CY);
  effectRipple.Center := FCenter;
  
  FDirX := 3;
  FDirY := 3;
end;

class procedure TfrmAbout.ShowSelf(const iBmp: TBitmap);
begin
  var Form := TfrmAbout.Create(nil);
  try
    Form.SetBitmap(iBmp);
    Form.ShowModal;
  finally
    Form.DisposeOf;
  end;
end;

end.
