﻿unit uUncyclopediaUtils;

interface

uses
  System.SysUtils;
type
  TUncyclopediaUtils = class
  private const
    PAGE_URL = 'https://ansaikuropedia.org/wiki/';
    API_URL = 'ja.uncyclopedia.info/api.php';
    PARAM_FORMAT = 'format';
    VALUE_FORMAT_JSON = 'json';
    PARAM_ACTION = 'action';
    VALUE_ACTION_QUERY = 'query';
    PARAM_RN_NAMESPACE = 'rnnamespace';
    VALUE_RN_NAMESPACE_0 = '0';
    PARAM_LIST = 'list';
    VALUE_LIST_RANDOM = 'random';
  private type
    {$HINTS OFF}
    TContinue = class
    private var
      RnContinue: String;
      Continue: String;
    end;
    TPage = class
    private var
      Id: UInt64;
      NS: UInt64;
      Title: String;
    end;
    TQuery = class
    private var
      Random: TArray<TPage>;
    end;
    TRandomResult = class
    private var
      BatchComplete: String;
      Continue: TContinue;
      Query: TQuery;
    end;
    {$HINTS ON}
  public
    class procedure OpenRandomPage(const iOpened: TProc);
  end;

implementation

uses
  System.Net.URLClient
  , PK.Net.JSON.Loader
  , PK.Utils.Browser
  ;

{ TUncyclopediaUtils }

class procedure TUncyclopediaUtils.OpenRandomPage(const iOpened: TProc);
begin
  var URI: TURI;
  URI.Scheme := TURI.SCHEME_HTTPS;
  URI.Host := API_URL;
  URI.AddParameter(PARAM_FORMAT, VALUE_FORMAT_JSON);
  URI.AddParameter(PARAM_ACTION, VALUE_ACTION_QUERY);
  URI.AddParameter(PARAM_RN_NAMESPACE, VALUE_RN_NAMESPACE_0);
  URI.AddParameter(PARAM_LIST, VALUE_LIST_RANDOM);

  var Loader := TJsonLoader<TRandomResult>.Create;
  Loader.Execute(
    URI.ToString,
    True,
    procedure (const iSuccess: Boolean; const iResult: TRandomResult)
    begin
      if
        iSuccess and
        (iResult.Query <> nil) and
        (Length(iResult.Query.Random) > 0)
      then
        OpenBrowser(PAGE_URL + iResult.Query.Random[0].Title);

      Loader.DisposeOf;

      if Assigned(iOpened) then
        iOpened;
    end
  );
end;

end.
