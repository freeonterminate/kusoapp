﻿(*
 * KusoApp
 *
 * PLATFORMS
 *   Windows / macOS
 *
 * 2019/12/01 Version 1.0
 * Programmed by pik
 *)

program KusoApp;

uses
  System.StartUpCopy,
  FMX.Forms,
  uMain in 'uMain.pas' {frmMain},
  uAbout in 'uAbout.pas' {frmAbout},
  uUncyclopediaUtils in 'uUncyclopediaUtils.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TfrmMain, frmMain);
  Application.Run;
end.
