﻿(*
 * KusoApp
 *
 * PLATFORMS
 *   Windows / macOS
 *
 * 2019/12/01 Version 1.0
 * Programmed by pik
 *)

unit uMain;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.Ani,
  FMX.Effects, FMX.Objects, FMX.Menus, FMX.Filter.Effects, FMX.Layouts,
  PK.TrayIcon, PK.Utils.Form, PK.IniFile;

type
  TfrmMain = class(TForm)
    txtUnko: TText;
    effectGlow: TGlowEffect;
    animGlowColor: TColorKeyAnimation;
    popupContext: TPopupMenu;
    menuAbout: TMenuItem;
    menuSep1: TMenuItem;
    menuExit: TMenuItem;
    animGetOut: TFloatAnimation;
    animGetOutOpacity: TFloatAnimation;
    effectGetOut: TBandedSwirlEffect;
    animRotate: TFloatAnimation;
    procedure txtUnkoDblClick(Sender: TObject);
    procedure txtUnkoMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
    procedure menuExitClick(Sender: TObject);
    procedure menuAboutClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure animGetOutFinish(Sender: TObject);
  private const
    INI_SECTION_POSITION = 'position';
    INI_KEY_LEFT = 'left';
    INI_KEY_TOP = 'top';
  private var
    FUnkoBmp: TBitmap;
    FIni: TXplatIniFile;
    FFormUtils: TFormUtils;
    FTrayIcon: TTrayIcon;
  private
    procedure TrayIconClick(Sender: TObject);
  end;

var
  frmMain: TfrmMain;

implementation

{$R *.fmx}

uses
  uAbout, uUncyclopediaUtils;

procedure TfrmMain.animGetOutFinish(Sender: TObject);
begin
  FIni.WriteInteger(INI_SECTION_POSITION, INI_KEY_LEFT, Left);
  FIni.WriteInteger(INI_SECTION_POSITION, INI_KEY_TOP, Top);

  Close;
end;

procedure TfrmMain.FormCreate(Sender: TObject);
const
  UNKO_TEXT = '💩';
begin
  FIni := CreateIniFile('pik');

  Left := FIni.ReadInteger(INI_SECTION_POSITION, INI_KEY_LEFT, Left);
  Top := FIni.ReadInteger(INI_SECTION_POSITION, INI_KEY_TOP, Top);

  FFormUtils := TFormUtils.Create;
  FFormUtils.HideTaskbar;

  FTrayIcon := TTrayIcon.Create;

  FTrayIcon.AssignPopupMenu(popupContext);
  FTrayIcon.RegisterOnClick(TrayIconClick);

  // TrayIcon に表示する画像の作成
  FUnkoBmp := TBitmap.Create;
  var FontSize := txtUnko.TextSettings.Font.Size;

  FUnkoBmp.Canvas.Font.Size := FontSize;
  var W := Trunc(FUnkoBmp.Canvas.TextWidth(UNKO_TEXT));
  var H := Trunc(FUnkoBmp.Canvas.TextHeight(UNKO_TEXT));
  var M := -H div 4 + 8;
  H := H + M;
  FUnkoBmp.SetSize(W, H);

  FUnkoBmp.Canvas.BeginScene;
  try
    FUnkoBmp.Canvas.Font.Size := FontSize;
    FUnkoBmp.Canvas.FillText(
      RectF(0, M, W, H),
      UNKO_TEXT,
      False,
      1,
      [],
      TTextAlign.Leading);
  finally
    FUnkoBmp.Canvas.EndScene;
  end;

  FTrayIcon.RegisterIcon('Norm', FUnkoBmp);
  FTrayIcon.Apply;
  FTrayIcon.ChangeIcon('Norm', UNKO_TEXT);
end;

procedure TfrmMain.FormDestroy(Sender: TObject);
begin
  FTrayIcon.DisposeOf;
  FFormUtils.DisposeOf;
  FUnkoBmp.DisposeOf;
end;

procedure TfrmMain.menuAboutClick(Sender: TObject);
begin
  FFormUtils.BringToFront;
  TfrmAbout.ShowSelf(FUnkoBmp);
end;

procedure TfrmMain.menuExitClick(Sender: TObject);
begin
  FFormUtils.BringToFront;

  effectGlow.Enabled := False;
  effectGetOut.Enabled := True;

  animGetOut.Start;
  animGetOutOpacity.Start;
end;

procedure TfrmMain.TrayIconClick(Sender: TObject);
begin
  FFormUtils.BringToFront;
end;

procedure TfrmMain.txtUnkoDblClick(Sender: TObject);
begin
  animRotate.Start;

  TUncyclopediaUtils.OpenRandomPage(
    procedure
    begin
      animRotate.Stop;
    end
  );
end;

procedure TfrmMain.txtUnkoMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Single);
begin
  StartWindowDrag;
end;

end.
